//Ex1
function calcExcercise1() {
  var username = document.getElementById("username").value;
  var tong_thu_nhap_nam =
    document.getElementById("tong-thu-nhap-nam").value * 1;
  var so_nguoi_phu_thuoc =
    document.getElementById("so-nguoi-phu-thuoc").value * 1;
  var result;
  if (tong_thu_nhap_nam > 0) {
    if (tong_thu_nhap_nam <= 60e6) {
      result = (tong_thu_nhap_nam - 4e6 - so_nguoi_phu_thuoc * 1.6e6) * 0.05;
      if (result > 0) {
        document.getElementById("result-ex1").style.display = "block";
        document.getElementById(
          "result-ex1"
        ).innerHTML = `Họ tên: ${username}; Tiền thuế thu nhập cá nhân: ${result.toLocaleString()} VND`;
        // console.log(result.toLocaleString());
      } else {
        alert("Số tiền thu nhập không hợp lệ");
        document.getElementById("result-ex1").style.display = "none";
      }
    } else if (tong_thu_nhap_nam <= 120e6) {
      result = (tong_thu_nhap_nam - 4e6 - so_nguoi_phu_thuoc * 1.6e6) * 0.1;
      if (result > 0) {
        document.getElementById("result-ex1").style.display = "block";
        document.getElementById(
          "result-ex1"
        ).innerHTML = `Họ tên: ${username}; Tiền thuế thu nhập cá nhân: ${result.toLocaleString()} VND`;
        // console.log(result.toLocaleString());
      } else {
        alert("Số tiền thu nhập không hợp lệ");
        document.getElementById("result-ex1").style.display = "none";
      }
    } else if (tong_thu_nhap_nam <= 210e6) {
      result = (tong_thu_nhap_nam - 4e6 - so_nguoi_phu_thuoc * 1.6e6) * 0.15;
      if (result > 0) {
        document.getElementById("result-ex1").style.display = "block";
        document.getElementById(
          "result-ex1"
        ).innerHTML = `Họ tên: ${username}; Tiền thuế thu nhập cá nhân: ${result.toLocaleString()} VND`;
        // console.log(result.toLocaleString());
      } else {
        alert("Số tiền thu nhập không hợp lệ");
        document.getElementById("result-ex1").style.display = "none";
      }
    } else if (tong_thu_nhap_nam <= 384e6) {
      result = (tong_thu_nhap_nam - 4e6 - so_nguoi_phu_thuoc * 1.6e6) * 0.2;
      if (result > 0) {
        document.getElementById("result-ex1").style.display = "block";
        document.getElementById(
          "result-ex1"
        ).innerHTML = `Họ tên: ${username}; Tiền thuế thu nhập cá nhân: ${result.toLocaleString()} VND`;
        // console.log(result.toLocaleString());
      } else {
        alert("Số tiền thu nhập không hợp lệ");
        document.getElementById("result-ex1").style.display = "none";
      }
    } else if (tong_thu_nhap_nam <= 624e6) {
      result = (tong_thu_nhap_nam - 4e6 - so_nguoi_phu_thuoc * 1.6e6) * 0.25;
      if (result > 0) {
        document.getElementById("result-ex1").style.display = "block";
        document.getElementById(
          "result-ex1"
        ).innerHTML = `Họ tên: ${username}; Tiền thuế thu nhập cá nhân: ${result.toLocaleString()} VND`;
        // console.log(result.toLocaleString());
      } else {
        alert("Số tiền thu nhập không hợp lệ");
        document.getElementById("result-ex1").style.display = "none";
      }
    } else if (tong_thu_nhap_nam <= 960e6) {
      result = (tong_thu_nhap_nam - 4e6 - so_nguoi_phu_thuoc * 1.6e6) * 0.3;
      if (result > 0) {
        document.getElementById("result-ex1").style.display = "block";
        document.getElementById(
          "result-ex1"
        ).innerHTML = `Họ tên: ${username}; Tiền thuế thu nhập cá nhân: ${result.toLocaleString()} VND`;
        // console.log(result.toLocaleString());
      } else {
        alert("Số tiền thu nhập không hợp lệ");
        document.getElementById("result-ex1").style.display = "none";
      }
    } else {
      result = (tong_thu_nhap_nam - 4e6 - so_nguoi_phu_thuoc * 1.6e6) * 0.35;
      if (result > 0) {
        document.getElementById("result-ex1").style.display = "block";
        document.getElementById(
          "result-ex1"
        ).innerHTML = `Họ tên: ${username}; Tiền thuế thu nhập cá nhân: ${result.toLocaleString()} VND`;
        // console.log(result.toLocaleString());
      } else {
        alert("Số tiền thu nhập không hợp lệ");
        document.getElementById("result-ex1").style.display = "none";
      }
    }
  } else {
    alert("Số tiền thu nhập không hợp lệ");
  }
}
//ex2
function myChoose() {
  if (document.getElementById("mySelect").value == "2") {
    document.getElementById("so-ket-noi-disable").style.display = "block";
  } else {
    document.getElementById("so-ket-noi-disable").style.display = "none";
  }
}
function calcExcercise2() {
  var type = document.getElementById("mySelect").value;
  var ma_khach_hang = document.getElementById("ma-khach-hang").value;
  var so_kenh_cao_cap = document.getElementById("so-kenh-cao-cap").value * 1;

  if (type == 0) {
    alert("Hãy chọn loại khách hàng");
    document.getElementById("result-ex2").style.display = "none";
  }
  if (type == 1) {
    document.getElementById("result-ex2").style.display = "block";
    result = 25 + 7.5 * so_kenh_cao_cap;
    document.getElementById(
      "result-ex2"
    ).innerHTML = `Mã khách hàng: ${ma_khach_hang}; Tiền cáp: $${result}`;
    // console.log(result);
  }
  if (type == 2) {
    document.getElementById("result-ex2").style.display = "block";
    var so_ket_noi = document.getElementById("so-ket-noi").value * 1;
    // console.log("so ket noi", so_ket_noi);
    if (so_ket_noi >= 0) {
      if (so_ket_noi <= 10) {
        result = 90 + 50 * so_kenh_cao_cap;
        document.getElementById(
          "result-ex2"
        ).innerHTML = `Mã khách hàng: ${ma_khach_hang}; Tiền cáp: $${result}`;
        // console.log(result);
      } else {
        result = 90 + (so_ket_noi - 10) * 5 + 50 * so_kenh_cao_cap;
        document.getElementById(
          "result-ex2"
        ).innerHTML = `Mã khách hàng: ${ma_khach_hang}; Tiền cáp: $${result}`;
        // console.log(result);
      }
    } else {
      alert("Số kết nối phải >=0");
    }
  }
}
